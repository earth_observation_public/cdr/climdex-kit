
Query the pixel-by-pixel spread of the ensamble (difference between
maximum and minimum values among the included models) of the
values averaged in a given time interval, given a specific emissions
scenario, and over the whole area of interest.

This is a proxy to assess the uncertainty of the model: the more
compact the spread, the more certain a prediction.

