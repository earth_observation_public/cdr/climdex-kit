
Query the pixel-by-pixel number (count) of models in the ensemble
where the difference between the values averaged over two time intervals
of a same emissions scenario is greater than 0, over a given
area of interest.

This is a proxy to verify whether the accordance of a given trend
(increasing trend in this case, otherwise test for difference < 0)
within all the models of the ensemble.

