
Query the 1D time-series of the both spatial- and model-averaged
values of a given emissions scenario, of the whole area of interest.

