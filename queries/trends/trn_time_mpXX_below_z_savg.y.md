
Query the 1D time-series of the spatially-averaged 95-th percentiles
of the models ensemble for those pixels below a given elevation threshold,
for a given emissions scenario, in the whole area of interest.

*Since the percentile operation is not available in the WCPS set
of expressions, the query loads the data for each time-step in an
array, to be later processed to get the percentile values. 

