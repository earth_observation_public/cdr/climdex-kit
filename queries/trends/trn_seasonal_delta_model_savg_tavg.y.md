
Query the 1D series of temperature change between two time intervals
and for a given emissions scenario, for each model in an ensamble,
averaging out the spatial and temporal dimensions,
for the whole area of interest.

