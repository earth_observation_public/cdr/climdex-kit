
Query the pixel-by-pixel difference between two emissions scenarios
of the averages of two identical time intervals of the ensemble averages,
over the whole area of interest.

