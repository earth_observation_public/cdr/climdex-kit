
Query a pixel-by-pixel average of a given time interval of the ensemble averages,
for a single emissions scenario, and over the whole area of interest.

