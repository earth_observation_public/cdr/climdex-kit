
Query the pixel-by-pixel difference between the averages of two time intervals
of the ensemble averages for a given emissions scenario,
over the whole area of interest.

