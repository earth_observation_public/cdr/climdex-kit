
Query a single time-step map of the ensemble averages,
and a single emissions scenario, and over a specific bounding box.

