
Query a single (raw) time-step map, from a single model of the ensemble,
and a single emissions scenario, and over a specific bounding box.

